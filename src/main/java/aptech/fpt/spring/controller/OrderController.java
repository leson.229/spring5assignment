package aptech.fpt.spring.controller;

import aptech.fpt.spring.entity.Order;
import aptech.fpt.spring.model.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class OrderController {

    @Autowired
    OrderRepository orderRepository;

    @RequestMapping(path = "/order/list", method = RequestMethod.GET)
    public String getListOrder(Model model) {
        List<Order> orders =(List<Order>) orderRepository.findAll();
        model.addAttribute("orders", orders);
        return "order-list";
    }

    @RequestMapping(value = "/order/add")
    public String addOrder(Model model) {
        model.addAttribute("order", new Order());
        return "order-form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(Order order) {
        orderRepository.save(order);
        return "redirect:/";
    }

    @RequestMapping(value = "/order/edit", method = RequestMethod.GET)
    public String editOrder(@RequestParam("id") Integer orderId, Model model) {
        Optional<Order> orderEdit = orderRepository.findById(orderId);
        orderEdit.ifPresent(order -> model.addAttribute("order", order));
        return "order-edit";
    }

    @RequestMapping(value = "/order/delete", method = RequestMethod.GET)
    public String deleteOrder(@RequestParam("id") Integer orderId, Model model) {
        orderRepository.deleteById(orderId);
        return "redirect:/";
    }

}
